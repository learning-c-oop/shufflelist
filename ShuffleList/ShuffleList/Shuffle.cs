﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShuffleList
{
    class Shuffle
    {
        public void ShuffleList(List<int> list)
        {
            Random rnd = new Random();
            for (int i = 0; i < list.Count; i++)
            {
                int rndIndex = rnd.Next(0, list.Count);
                SwapListItems(list, i, rndIndex);
            }
        }
        public void SwapListItems(List<int> list, int index1, int index2)
        {
            int temp = list[index1];
            list[index1] = list[index2];
            list[index2] = temp;
        }
    }
}
