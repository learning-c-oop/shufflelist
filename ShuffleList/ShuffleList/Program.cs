﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShuffleList
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = CreateRandomList(10);
            PrintList(list);
            Console.WriteLine();
            Shuffle listShuffle = new Shuffle();
            listShuffle.ShuffleList(list);
            PrintList(list);

            Console.ReadKey();
        }
        static List<int> CreateRandomList(int count)
        {
            List<int> list = new List<int>(count);
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                list.Add(rnd.Next(1, 100));
            }
            return list;
        }
        static void PrintList(List<int> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }
        }
    }
}
